package com.epam;

/**
 * Represents an Exception.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */
public class InvalidCommandException extends Throwable {
    private String message;

    /**
     * Message about error.
     *
     * @param s key menu
     */
    public InvalidCommandException(final String s) {

        message = s + " isn`t correct number.";
    }

    /**
     * This is method print message about error.
     */
    public void printMessage() {

        System.out.println(message);
    }
}
