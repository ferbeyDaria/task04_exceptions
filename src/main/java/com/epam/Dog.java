package com.epam;

/**
 * Represents an dog class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */
public class Dog extends Animals {
    private int angerLevel;

    public Dog(String name, String color, int angerLevel) {
        super(name, color);
        this.angerLevel = angerLevel;
    }

    public int getAngerLevel() {
        return angerLevel;
    }

    public String sound() {
        return "gav-gav";
    }
}
