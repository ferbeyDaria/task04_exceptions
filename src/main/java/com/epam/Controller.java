package com.epam;

/**
 * Represents an view of application.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */
public class Controller {
    private BisnessLogical bl;

    public Controller() {
        bl = new BisnessLogical();
    }

    public void printDog() {
        bl.showDog(bl.getDog());
    }

    public void printCat() {
        bl.showCat(bl.getCat());
    }

    public void printCatCute() {
        bl.printСute(bl.getCat());
    }
}
