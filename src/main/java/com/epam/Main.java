package com.epam;

/**
 * Represents an view of application.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */

public class Main {
    public static void main(String[] args) {
        new View().show();

    }
}
