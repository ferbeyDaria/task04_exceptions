package com.epam;

/**
 * Represents an cat class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */
public class Cat extends Animals {
    public int levelCute;

    public Cat(String name, String color, int levelCute) {
        super(name, color);
        this.levelCute = levelCute;
    }

    public int getLevelCute() {
        return levelCute;
    }

    @Override
    public String sound() {
        return "myau-myau";
    }
}
