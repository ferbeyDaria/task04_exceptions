package com.epam;

/**
 * Represents an animals abstract class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */
public abstract class Animals {
    private String name;
    private String color;

    public Animals(String name, String color) {
        this.name = name;
        this.color = color;g
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public abstract String sound();
}
