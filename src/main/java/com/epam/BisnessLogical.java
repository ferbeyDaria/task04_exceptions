package com.epam;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Represents an BisnessLogical.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */
public class BisnessLogical implements AutoCloseable {
    private Dog dog;
    private Cat cat;
    private PrintWriter fileDog;
    private PrintWriter fileCat;

    public BisnessLogical() {
        dog = new Dog("Bobik", "black", 50);
        cat = new Cat("Mars", "rude", 100);
    }

    /**
     * This is class show information about dogs.
     *
     * @param dog Dog
     */
    public void showDog(Dog dog) {
        System.out.println("Name: " + dog.getName() + "\n" + "Color:" + dog.getColor() + "\n" + "Anger level: " + dog.getAngerLevel() + "\n");
        try {
            fileDog = new PrintWriter(this.dog.getName() + ".txt");
            fileDog.println("Name: " + dog.getName() + "\n" + "Color:" + dog.getColor() + "\n" + "Anger level: " + dog.getAngerLevel());
        } catch (FileNotFoundException e) {
            System.out.println("File not created!");
        }
    }

    /**
     * This is method show information about cat.
     *
     * @param cat Cat
     */
    public void showCat(Cat cat) {
        System.out.println("Name: " + cat.getName() + "\n" + "Color: " + cat.getColor() + "\n" + "Cute: " + cat.getLevelCute());
        try {
            fileCat = new PrintWriter(this.cat.getName() + ".txt");
            fileCat.println("Name: " + cat.getName() + "\n" + "Color: " + cat.getColor() + "\n" + "Cute: " + cat.getLevelCute());
        } catch (FileNotFoundException e) {
            System.out.println("File not created!");
        }
    }

    /**
     * This is method show level cute cate.
     *
     * @param cat Cat
     */
    public void printСute(Cat cat) {
        if (cat.getLevelCute() == 0) {
            System.out.println(cat.getName() + " isn`t cute!Рug the cat");
        } else if (cat.getLevelCute() > 0 && cat.getLevelCute() <= 50) {
            System.out.println(cat.getName() + " isn`t cute, play with him");
        } else {
            System.out.println(cat.getName() + " is CUTE!");
        }
    }

    public Dog getDog() {
        return dog;
    }

    public Cat getCat() {
        return cat;
    }

    /**
     * @throws IOException
     */
    public void close() throws IOException {
        if (cat.getLevelCute() < 50) {
            throw new IOException("Exception: method close()");
        }
        if (fileDog != null && fileCat != null) {
            fileDog.println();
            fileCat.println();
            fileDog.close();
            fileCat.close();
        }
    }
}
