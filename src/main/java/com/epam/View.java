package com.epam;

import java.util.Scanner;

/**
 * Represents a view of application.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-14
 */
public class View {
    public Controller controller;
    public static Scanner scan = new Scanner(System.in);

    public View() {
        controller = new Controller();
    }

    /**
     * This is method print munu.
     */
    private void outputMenu() {
        System.out.println("    Helper");
        System.out.println("1 - print information about dog");
        System.out.println("2 - print information about cat");
        System.out.println("3 - print cute cat level");
        System.out.println("Q - exit");
    }

    /**
     * This is method output menu and print information.
     */
    public void show() {
        String key;
        outputMenu();
        do {
            System.out.print("Please enter number: ");
            key = scan.nextLine();
            try {
                switch (key) {
                    case "1":
                        controller.printDog();
                        break;
                    case "2":
                        controller.printCat();
                        break;
                    case "3":
                        controller.printCatCute();
                    case "Q":
                        break;
                    default:
                        throw new InvalidCommandException(key);
                }
            } catch (InvalidCommandException e) {
                e.printMessage();
            }
        } while (!key.equals("Q"));
    }
}
